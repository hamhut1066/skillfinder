(function() {
    angular.module('mydevs')
        .factory('Toggle', function() {
            return {
                toggle: function(active) {
                    if (active) {
                        //animate down to #search TODO
                        $("#loadingModal").modal();
                        $("#search-devs").attr('disabled','');
                        //set searching div to visible
                    } else {
                        $("#search-devs").removeAttr('disabled')
                            $("#loadingModal").modal('hide');
                        //set searching div to invisible
                    }
                },
                nodevs: function(nodevs) {
                    if (nodevs) {
                        var html_data = '<div class="container"> <p>No devs match the criteria that you have entered</p> </div>';
                        $("#no-devs").html(html_data);
                    } else {
                        $("#no-devs").empty();
                    }
                }
            }

        })
})();
