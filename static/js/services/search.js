(function() {
    angular.module('mydevs')
        .service('Search', function($http) {
            var local_location = '';
            var local_langs = [];
            var devs = "{}";
            var new_data = false;
            function getData(id) {
                $http.get('/api/fetch/'+id)
                    .success(function(data) {
                        if(data.state === "SUCCESS") {
                            devs = data.items;
                            new_data = !new_data;
                            //TODO add broadcast so model updates
                        }
                        else {
                            //console.log(id);
                            setTimeout(function() { return getData(id) }, 1000);
                        }
                    })
                .error(function(data) {
                    console.log(data);
                })
            }
            function fetchUsers(location, langs) {
                //TODO add checking for pcresence of vars
                if (local_location === location && JSON.stringify(local_langs) == JSON.stringify(langs)) { new_data = !new_data; return 0; } //TODO find some way to return a scrollTo
                local_location = location;
                local_langs = langs.slice(0);
                //TODO add the lowering of the modal... or maybe another indicator that results are being fetched
                var url = "/search/"+location+"?";
                for (var i = 0; i < langs.length; i++) {
                    url += "&lang="+langs[i];
                }
                $http.get(url)
                    .success(function(data) {
                        //TODO start looping for the actual data
                        getData(data.id)
                    })
                .error(function(data) {
                    console.log(data)
                });
            }

            this.findUsers = function(location, langs) { fetchUsers(location, langs); }
            this.getUsers = function() { 
                return JSON.parse(devs) || {}  
            }
            this.changed = function() { return new_data; }
        });
})();
