(function() {
    angular.module('mydevs')
        .service('Map', function() {
            var map;

            function initialize() {
                var mapOptions = {
                    center: new google.maps.LatLng(50,0),
                    zoom: 3,
                    scrollWheel: false,
                    disableDefaultUI: true,
                    scrollwheel: false,
                    navigationControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    draggable: false,
                }
                // map = new google.maps.Map(document.getElementById('devmap'), mapOptions);
                map;
            }

            // google.maps.event.addDomListener(window, 'load', initialize);

            this.setLocation = function(place) {
                return 0;
                var g = new google.maps.Geocoder();
                g.geocode({address: place}, function(ret) {
                    try {
                        var x = ret[0].geometry.viewport;
                        map.fitBounds(x);
                    } catch(e) { console.error(e); }

                })
            }
        });

})();
