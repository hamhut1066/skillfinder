angular.module("mydevs", []);

var manipulate = {
    scrollTo: function(location, time) {
        setTimeout(function() {
            $("html, body").animate({
                scrollTop: $(location).offset().top
            }, 2e3);
        }, time);
    }
};

(function() {
    angular.module("mydevs").controller("DevController", [ "$scope", "$sce", "Map", "Search", "Toggle", function($scope, $sce, Map, Search, Toggle) {
        $scope.location = "Edinburgh";
        $scope.langs = ['javascript'];
        $scope.devs = Search.getUsers();
        $scope.modal = {};
        $scope.search = function() {
            Map.setLocation($scope.location);
            Search.findUsers($scope.location, $scope.langs);
            Toggle.toggle(1);
            manipulate.scrollTo("#search", 500);
        };
        $scope.$watch(Search.changed, function() {
            $scope.devs = Search.getUsers();
            Toggle.toggle(0);
        });
        $scope.devModal = function(input) {
            $scope.modal = $scope.devs[input];
            $scope.modal["about_trusted"] = {};
            for (var key in $scope.modal.about) {
                $scope.modal["about_trusted"][key] = $sce.trustAsHtml($scope.modal.about[key]);
            }
            $("#dev-modal").modal();
        };
        $scope.addLang = function() {
            $scope.langs.push($scope.newLang);
            $scope.newLang = "";
        };
        $scope.removeLang = function(lang) {
            var old = $scope.langs;
            $scope.langs = [];
            angular.forEach(old, function(l) {
                if (l !== lang) {
                    $scope.langs.push(l);
                }
            });
        };
    } ]);
})();

(function() {
    angular.module("mydevs").service("Map", function() {
        var map;
        function initialize() {
            var mapOptions = {
                center: new google.maps.LatLng(50, 0),
                zoom: 3,
                scrollWheel: false,
                disableDefaultUI: true,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
                scaleControl: false,
                draggable: false
            };
            // map = new google.maps.Map(document.getElementById("devmap"), mapOptions);
        }
        //google.maps.event.addDomListener(window, "load", initialize);
        this.setLocation = function(place) {
            return 0;
            var g = new google.maps.Geocoder();
            g.geocode({
                address: place
            }, function(ret) {
                try {
                    var x = ret[0].geometry.viewport;
                    map.fitBounds(x);
                } catch (e) {
                    console.error(e);
                }
            });
        };
    });
})();

(function() {
    angular.module("mydevs").service("Search", function($http) {
        var local_location = "";
        var local_langs = [];
        var devs = "{}";
        var new_data = false;
        function getData(id) {
            $http.get("/api/fetch/" + id).success(function(data) {
                if (data.state === "SUCCESS") {
                    devs = data.items;
                    new_data = !new_data;
                } else {
                    setTimeout(function() {
                        return getData(id);
                    }, 1e3);
                }
            }).error(function(data) {
                console.log(data);
            });
        }
        function fetchUsers(location, langs) {
            if (local_location === location && JSON.stringify(local_langs) == JSON.stringify(langs)) {
                new_data = !new_data;
                return 0;
            }
            local_location = location;
            local_langs = langs.slice(0);
            var url = "/search/" + location + "?";
            for (var i = 0; i < langs.length; i++) {
                url += "&lang=" + langs[i];
            }
            $http.get(url).success(function(data) {
                getData(data.id);
            }).error(function(data) {
                console.log(data);
            });
        }
        this.findUsers = function(location, langs) {
            fetchUsers(location, langs);
        };
        this.getUsers = function() {
            return JSON.parse(devs) || {};
        };
        this.changed = function() {
            return new_data;
        };
    });
})();

(function() {
    angular.module("mydevs").factory("Toggle", function() {
        return {
            toggle: function(active) {
                if (active) {
                    $("#loadingModal").modal();
                    $("#search-devs").attr("disabled", "");
                } else {
                    $("#search-devs").removeAttr("disabled");
                    $("#loadingModal").modal("hide");
                }
            },
            nodevs: function(nodevs) {
                if (nodevs) {
                    var html_data = '<div class="container"> <p>No devs match the criteria that you have entered</p> </div>';
                    $("#no-devs").html(html_data);
                } else {
                    $("#no-devs").empty();
                }
            }
        };
    });
})();
