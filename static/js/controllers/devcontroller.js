(function() {
    angular.module('mydevs')
        .controller('DevController', ['$scope', '$sce', 'Map','Search', 'Toggle',  function($scope, $sce, Map, Search, Toggle) {
            $scope.location = "aoeu";
            $scope.langs = [];
            $scope.devs = Search.getUsers();
            $scope.modal = {};

            $scope.search = function() { Map.setLocation($scope.location); Search.findUsers($scope.location, $scope.langs); Toggle.toggle(1); manipulate.scrollTo("#search", 500); } 

            $scope.$watch(Search.changed, function() { $scope.devs = Search.getUsers(); Toggle.toggle(0);});



            $scope.devModal = function(input) {
                $scope.modal = $scope.devs[input];
                $scope.modal['about_trusted'] = {};
                for (var key in $scope.modal.about) {
                    $scope.modal['about_trusted'][key] = $sce.trustAsHtml($scope.modal.about[key])
                }
                //$scope.modal['about_trusted'] = $sce.trustAsHtml($scope.modal.about)
                $("#dev-modal").modal()
            }
            //dealing with the language interface!!
            //TODO move into directive!!
            $scope.addLang = function() {
                //TODO add support to stop duplicates
                $scope.langs.push($scope.newLang);
                $scope.newLang = '';
            };
            $scope.removeLang = function(lang) {
                var old = $scope.langs;
                $scope.langs = [];
                angular.forEach(old, function(l) {
                    if (l !== lang) { $scope.langs.push(l); }
                });
            };
        }]);
})();
