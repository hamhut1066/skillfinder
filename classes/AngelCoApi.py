from classes.Api import Api
from classes.Developer import Developer
class AngelList(Api):
    def __init__(self):
        self.base_url = "https://api.angel.co/1/users/search"
        self.provider = "angellist"

    def getUsers(self, location, langs):
        raise Exception("Implement AngelList getUsers function!!")
        return 0

    def getUser(self, name):
        url = "%s?slug=%s" % (self.base_url, name)
        ret = super().call(url, 1)
        prov_specific = {}
        d = Developer('')
        if 'id' in ret.keys():
            d = Developer(name)
            #user exists
            d.add_url(ret['angellist_url'], self.provider)

            #add url's
            if ret['blog_url'] != "":
                d.add_url(ret['blog_url'], 'blog')
            if ret['twitter_url'] != "":
                d.add_url(ret['twitter_url'], "twitter")
            if ret['linkedin_url'] != "":
                d.add_url(ret['linkedin_url'], "linkedin")
            if ret['github_url'] != "":
                d.add_url(ret['github_url'], "github")

            d.add_about(ret['bio'], self.provider)

            if 'skills' in ret.keys():
                prov_specific['skills'] = ret['skills']
            if 'what_ive_built' in ret.keys():
                prov_specific['what_ive_built'] = ret['what_ive_built']
            d.add_provider(prov_specific, self.provider)
        #construct user
        return d
