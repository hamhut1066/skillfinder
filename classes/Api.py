import requests
from classes.Developer import Developer
import redis
import json

class Api:
    '''the default class for api calls'''
    provider = ''
    base_url = ''
    devs = []
    redis = redis.StrictRedis(host='localhost', port=6379, db=0)
    timeout = 60 * 60 * 24 + 365 #24 hours

    def __init__(self):
        pass

    def call(self, url, js, auth=None):
        #first check whether exists in redis database
        red = self.redis.get(url)
        if red != None:
            return json.loads(red.decode())

        if auth:
            r = requests.get(url, auth=(auth[0], auth[1]))
        else:
            r = requests.get(url)


        self.redis.setex(url, self.timeout, r.text)
        if js:
            if '\ufeff' in r.text: #dirty hack to get rid of extra unicode character at the beginning of the string... TODO fix
                tmp = r.text()[1:]
                return json.loads(tmp)
            else:
                return r.json()
        else:
            return r.text

    def append(self, dev):
        Api.devs.append(dev)

    def log(self, a):
        if a:
            print("Start: %s##########################" % self.provider)
        else:
            print("Finish: %s##########################" % self.provider)
