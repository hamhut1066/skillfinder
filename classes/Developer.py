import urllib, hashlib
import html
from classes.helpers import strip_tags

class Developer:
    #user, email, profile url, avatar url, custom_urls
    def __init__(self, user):
        self.user = user.lower()
        self.real_name = self.user
        self.urls = {}
        self.email = {}
        self.avatar = 'http://en.gravatar.com/avatar/00000000000000000000000000000000?d=identicon'
        self.about = {}
        self.providers = {}

    def set_real_name(self, real_name):
        self.real_name = real_name

    def get_real_name(self):
        return self.real_name

    def get_email(self):
        return self.email

    def get_user(self):
        return self.user

    def get_urls(self):
        return self.urls

    def add_email(self, email, sha):
        _email = ''
        if sha:
            _email = hashlib.md5(email.lower().encode('utf-8')).hexdigest()
            self.email[_email] = 1
        else:
            _email = email
            self.email[email] = 1
        if '00000000000000000000000000000000' in self.avatar:
            self.avatar = "http://www.gravatar.com/avatar/%s?d=identicon" % self.email[_email]

    def add_url(self, url, provider):
        self.urls[provider] = url
    def set_avatar(self, url):
        self.avatar = url

    def add_about(self, about, provider):
        #TODO allow bios from multiple resources
        self.about[provider] = strip_tags(about)

    def get_about(self):
        return self.about

    def get_providers(self):
        return self.providers

    def add_provider(self, details, provider):
        #adds provider details to object
        self.providers[provider] = details


    def merge(self, dev):
        #add emails, add urls
        self.urls.update(dev.get_urls())
        self.email.update(dev.get_email())
        self.real_name = dev.get_real_name()

        for key in dev.get_about().keys():
            self.about[key] = dev.get_about()[key]

        for key in dev.get_providers().keys():
            self.providers[key] = dev.get_providers()[key]
        return self

    def __str__(self):
        return {'user': self.user, 'email': self.email, 'urls': self.urls, 'avatar': self.avatar, 'about': self.about, 'providers': self.providers, 'real_name': self.real_name}

    def toString(self):
        return "User: %s, Email: %s" % (self.user, self.email)
