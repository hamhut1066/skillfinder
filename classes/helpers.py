import requests
from random import shuffle
from html.parser import HTMLParser

def get_images():
    return ["/static/stylish-portfolio/img/bg.jpg","/static/stylish-portfolio/img/callout.jpg","/static/stylish-portfolio/img/portfolio-1.jpg","/static/stylish-portfolio/img/portfolio-2.jpg","/static/stylish-portfolio/img/portfolio-3.jpg","/static/stylish-portfolio/img/portfolio-4.jpg"]
    url = "https://api.flickr.com/services/rest?method=flickr.galleries.getPhotos&api_key=83256e64276f42a430a8eba5496dd687&gallery_id=124870730-72157644933632388&format=json&nojsoncallback=1&extras=url_z"
    a = requests.get(url).json()
    arr = []
    for i in a['photos']['photo']:
        arr.append(i['url_z'])
    shuffle(arr)
    return arr[:5]

class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    if html == None:
        return html
    else:
        s = MLStripper()
        s.feed(html)
        return s.get_data()
