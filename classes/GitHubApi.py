from classes.Api import Api
from classes.Developer import Developer
class GitHub(Api):
    def __init__(self):
        self.base_url = "https://api.github.com"
        self.provider = "github"
        self.auth = ('e687312895f64611c38bf5baf5c4b3f586923e70','')

    def getUsers(self, location, langs):
        url = "%s/search/users?q=type:User+" % self.base_url
        if (location != ""):
            url += "location:%s" % location
        for i in langs:
            url += "+language:%s" % i
        return self.normalise(super().call(url, 1, self.auth))

    def getUser(self, name):
        url = "%s/search/users?q=type:User+%s" % (self.base_url, name)
        ret = super().call(url, 1, self.auth)['items']
        if ( len(ret) == 0):
            return Developer('')
        user = ret[0]
        d = Developer(user['login'].lower())
        d.set_avatar(user['avatar_url'])
        d.add_email(user['gravatar_id'], 0)
        d.add_url(user['html_url'], self.provider)
        #TODO add github specific details
        return d

    def getKnownUser(self, user_url): #returns a dictionary with provider relevant information
        url = user_url
        repos = super().call(url, 1, self.auth)

        ret = {}
        ret['langs'] = []
        lang = {}
        for repo in repos:
            if repo['language'] == None:
                pass
            elif repo['language'] in lang.keys():
                lang[repo['language']] += 1
            else:
                lang[repo['language']] = 1
        for l in lang.keys():
            ret['langs'].append({'lang': l, 'count': lang[l]})
        return ret
    def normalise(self, users):
        #normalises data, for easy integration into the handler
        devs = []
        user_ = users #json.loads(users)
        if 'items' in user_:
            for i in user_['items']:
                tmp = Developer(i['login'])
                tmp.add_provider(self.getKnownUser(i['repos_url']), self.provider)
                tmp.add_email(i['gravatar_id'], 0)
                tmp.add_url(i['html_url'], "github")
                tmp.set_avatar(i['avatar_url'])
                devs.append(tmp)
                #TODO add github specific details
        return devs
