from classes.Api import Api
from classes.Developer import Developer
class StackOverflow(Api):
    #define global limits
    knows_lang = 3 #ratio of score to answers

    def __init__(self):
        self.base_url = "https://api.stackexchange.com/2.2"
        self.key = "KDNPQBUvH96uxjNGIUvBeQ(("
        self.filter = "!0ZJMqd*nycqVHsnj2VlIbU3h*"
        #https://api.stackexchange.com/2.2/users?order=desc&sort=reputation&inname=pangratz&site=stackoverflow&filter=!.L64Ihn5I4Pq9ERv4imMm)XX_O3iu
        self.provider = "stackoverflow"

    def get_users(self):
        #return all the users from stackoverflow as I cannot filter on location
        url = "%s/users.json" % self.base_url
        return super().call(url, 1)

    def get_tags(self, user_id):
        url = "%s/users/%s/tags?order=desc&sort=popular&site=stackoverflow&filter=%s&key=%s" % (self.base_url, user_id, self.filter, self.key)
        ret = super().call(url, 1)
        return ret['items']


    def getUser(self, name):
        url = "%s/users?site=stackoverflow&sort=reputation&order=desc&key=%s&filter=%s&inname=%s" % (self.base_url, self.key, self.filter, name)
        ret = super().call(url, 1)['items']
        #TODO seperate this out into a seperate function, which takes in data, and returns a developer
        if ( len(ret) == 0):
            return Developer('')
        user = ret[0]
        d = Developer(user['display_name'].lower())
        prov_specific = {}
        prov_specific['tags'] = self.get_tags(user['user_id'])
        prov_specific['question_count'] = user['question_count']
        prov_specific['answer_count'] = user['answer_count']
        prov_specific['reputation'] = user['reputation']

        d.add_provider(prov_specific, self.provider)
        d.set_avatar(user['profile_image'])
        if 'about_me' in user.keys():
            d.add_about(user['about_me'], self.provider)
        d.add_url(user['link'], self.provider)
        #TODO add stackoverflow specific details
        return d

    def getUsers(self, location, langs):
        #TODO Implement
        return []

    def normalise(self, users):
        pass
