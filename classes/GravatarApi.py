from Api import Api
class Gravatar(Api):
    def __init__(self, email_hash):
        self.base_url = "http://gravatar.com"
        self.provider = "gravatar"
        self.email_hash = email_hash

    def getUser(self):
        url = "%s/" % self.email_hash
        return self.normalise(super().call(url, 1))

    def normalise(self, user):
       print(user) 
