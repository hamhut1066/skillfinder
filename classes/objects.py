import requests
import json
import urllib, hashlib
#from Api import Api
from classes.GitHubApi import GitHub
from classes.StackOverflowApi import StackOverflow
from classes.AngelCoApi import AngelList
#from GravatarApi import Gravatar

class Handler:

    def __init__(self):
        self.objects = []
        self.meta_object = GitHub()
        self.objects.append(StackOverflow())
        self.objects.append(AngelList())

    def getUsers(self, location, langs):
        devs = dict()
        count = 0
        #get meta_object
        j = 0
        for mobj in self.meta_object.getUsers(location, langs):
            devs[j] = mobj
            j+=1
        devstmp = {}
        for obj in self.objects: #TODO see if I can do parallel
            for k, dev in devs.items():
                tmp = obj.getUser(dev.get_user())
                if dev.get_user() == tmp.get_user(): #TODO add better checking for user-alikeness
                    #same person
                    devs[k] = devs[k].merge(tmp) #possible error here
                elif tmp.get_user() != '':
                    devstmp[j] = tmp
                    j+=1
        #devs.update(devstmp)

        ret = {}
        #print("%s:%s" % (location, langs))
        for key,value in devs.items():
            ret[key] = devs[key].__str__()
        return json.dumps(ret)
