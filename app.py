from functools import wraps
from flask import Flask,request,render_template, redirect, jsonify, Response
from celery import Celery
#import requests
import settings
from classes.objects import Handler
from classes.helpers import get_images

app = Flask(__name__)
app.config.from_object(settings)

#basic auth
def check_auth(username, password):
    """this function is called to check the user is authenticated"""
    return username == "admin" and password == "somesecret"

def authenticate():
    """sends a 401 response for authentication"""
    return Response(
            'Could not verify your account credentials'
            '',401,
            {'WWW-Authenticate': 'Basic realm=Login Required'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated




def make_celery(app):
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery

celery = make_celery(app)


@celery.task(name="tasks.add")
def add(location, langs):
    api = Handler()
    #return "{'msg': 'you have reached your goal'}"
    return api.getUsers(location, langs)


@app.route('/')
def root():
    return redirect("/home", code=302)
    #return render_template('index.html')

@app.route('/home')
@requires_auth
def home():
    #add authorisation
    images = get_images()
    return render_template('home.html', images=images)

@app.route('/search')
def github():
    return "api stuff only please..."

@app.route('/search/<location>')
def search(location):
    langs = sorted(map(lambda x: x.lower(), request.args.getlist('lang'))) #sorts, and sets all to lowercase, in this way, I can cache results!
    res = add.apply_async((location, langs))
    context = {"id": res.id, "call": "getUsers"}
    return jsonify(context)

@app.route('/api/fetch/<id>')
def fetch(id):
    res = add.AsyncResult(id)
    if res.state == 'SUCCESS':
        return jsonify({"id": res.id, "items": res.result, "state": res.state})
    else:
        return jsonify({"id": res.id, "state": res.state}) 

if __name__ == '__main__':
    app.debug = True
    app.run(threaded=True)
